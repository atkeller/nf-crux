/* # MSConvert Nextflow Pipeline
 *
 * Params:
 *  --in_file_list: a text file with a raw file to be converted on each line
 *  --msconvert_config: an msconvert config file
 *  --tide_parameter_file: a crux parameter file specifying tide's params
 *  --database_file: either a fasta or a tide-index database file
*/

// Using a guid gives us flexibility to perform .join operations in more
//  complex pipelines.
def guid(path) {
    return path.replaceAll(/[:\/]/, "_")
}

// Parse in_file_list into individual file paths
Channel
    .fromPath( params.in_file_list )
    .splitText()
    .map{it.trim()}
    .map { tuple(guid(it), it, file(it)) }
    .set{ in_files }

msconvert_config = file( params.msconvert_config )
tide_params = file( params.tide_parameter_file )
tide_database_file = file( params.tide_database_file )

in_files.into { out_read_files__mapper; out_read_files__runner }

process file_mapper {

    /*
    Mapper allows for conditionally applying different options or
     processing to each file
    */

    container "python:3.7.3"

    input:
    set fileId, filePath, _ from out_read_files__mapper

    output:
    set fileId, stdout into out_file_mapper

    """
    #!/usr/bin/env python3

    file_path="$filePath"
    name = file_path.split('/')[-1]

    print("{0:s}".format(name))
    """
}

out_file_mapper.subscribe onNext: { println it }

/* Set environment variables for msconvert docker container
 **********************************************************/

// Turn off Wine debugging
msconvert_env_WINEDEBUG = Channel.value('-all')

/**********************************************************/

process msconvert {

    label 'bigTask'

    container 'chambm/pwiz-skyline-i-agree-to-the-vendor-licenses:3.0.19073-85be84641'

    memory '7168 MB'

    cpus 2

    input:
    set fileId, _, file(file) from out_read_files__runner
    file msc_config from msconvert_config
    env WINEDEBUG from msconvert_env_WINEDEBUG

    output:
    set fileId, file("msconvert_out/*") into out_msconvert

    // TODO set a reasonable output filename
    """
    wine msconvert -vz $file -c $msc_config --outdir msconvert_out
    """
}

process tide_search {

    container 'atkeller/crux:latest'
    // container 'maccosslab/crux-toolkit:latest'

    memory '32000 MB'

    cpus 36

    input:
    set msfileId, file(msfile) from out_msconvert
    file database_file from tide_database_file
    file parameter_file from tide_params

    output:
    set file('${msfileId}/crux-output/tide-search.target.pin'), file('crux-output/tide-search.target.sqt'), file('crux-output/tide-search.decoy.sqt'), file('crux-output/tide-search.params.txt'), file('crux-output/tide-search.log.txt') into out_tide_search

    """
    crux tide-search --parameter-file ${parameter_file} --output-dir crux-output --output_sqtfile 1 --output_percolatorfile 1 ${msfile} ${database_file}
    """
}

/*
process make_pin {

    container 'maccosslab/crux-pipeline:release-0.1.0'

    memory '15 GB'

    cpus 2

    input:

}
*/
