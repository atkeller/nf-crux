# Nextflow MSConvert Pipeline

## Test

```
nextflow crux.nf \
  -resume \
  -bucket-dir s3://atkeller-nextflow/runs/crux.nf/smoke_test \
  --in_file_list s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpcrux/smoke/msfiles.txt \
  --msconvert_config s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpcrux/smoke/msconvert_config.crux.txt \
  --tide_parameter_file s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpcrux/smoke/jarrett_high_high.params.txt \
  --tide_database_file s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpcrux/smoke/uniprot-human_no_tr.fasta
```
